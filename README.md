# Font Awesome for Adobe Muse

Usar Font Awesome en Adobe Muse nunca fue tan fácil, con este Widget usted podrá añadir impresionantes iconos a sus diseños, elegir el color, seleccionar uno de los 5 tamaños preestablecidos o darle el tamaño que desee a través de la herramienta de texto. Pero eso no es todo también podrás animar los iconos Spinner.

![](http://i.imgur.com/ORkTk0n.png)
[VER DEMO](http://www.leninalbertop.com.ve/demo/font-awesome/)

Esto es solo el código fuente, si desea descargar el widget para utilizarlo en sus diseños de Adobe Muse visite el [siguiente enlace](http://www.leninalbertop.com.ve/blog/font-awesome-excelente-widget-para-adobe-muse/).

Este Widget se ofrece de forma gratuita con licencia Creative Commons Atribución-No Comercial-Compartir Igual 4.0 Internacional (CC BY-NC-SA 4.0). Queda terminantemente prohibido el uso comercial del mismo. Si desea comunicarse conmigo por favor visite mi perfil en Twitter [@leninalbertop](http://twitter.com/leninalbertop).
